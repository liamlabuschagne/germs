import Game from "./obj/Game.js";

let game = new Game();

function loop(t) {
    game.loop();
    requestAnimationFrame(loop);
}
requestAnimationFrame(loop);