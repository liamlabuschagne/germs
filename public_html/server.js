const io = require('socket.io')(3000, {});
import Player from "./obj/Player.js";
import Dot from "./obj/Dot.js";

let dots = {};

for (let i = 0; i < 1000; i++) {
    dots["f" + i] = new Dot();
}

io.on('connection', socket => {
    dots[socket.id] = new Player();
    socket.emit("join", socket.id, dots);
    socket.broadcast.emit("new-player", socket.id, dots[socket.id]);

    function eat(i) {
        socket.broadcast.emit("eat", i);

        if (dots[i] && dots[i].vx == undefined) {
            dots[i] = new Dot();
            socket.broadcast.emit("new-dot", i, dots[i]);
            socket.emit("new-dot", i, dots[i]);
        } else {
            delete dots[i];
        }
    }

    socket.on("eat", (i) => {
        eat(i);
    });

    socket.on("update", (dot) => {
        dots[socket.id] = dot;
        socket.broadcast.emit("update", socket.id, dot);
    });

    socket.on("disconnect", () => {
        eat(socket.id);
    });

});