import Dot from "./Dot.js";

export default class Player extends Dot {
    colour = "red";
    r = 10;
    maxv = 5;
    vx = 0;
    vy = 0;

    constructor(obj = null) {
        super();
        if (obj) {
            obj && Object.assign(this, obj);
        }
        this.colour = "red";
    }

    update(game) {
        this.x -= this.vx;
        this.y -= this.vy;

        if (this.x < 0) {
            this.x = 0;
            this.vx = 0;
        } else if (this.x > game.WIDTH) {
            this.x = game.WIDTH;
            this.vx = 0;
        }

        if (this.y < 0) {
            this.y = 0;
            this.vy = 0;
        } else if (this.y > game.HEIGHT) {
            this.y = game.HEIGHT;
            this.vy = 0;
        }
    }

    draw(game) {
        game.ctx.fillStyle = this.colour;
        game.ctx.beginPath();
        game.ctx.ellipse((this.x * game.scale - game.xoffset), (this.y * game.scale - game.yoffset), this.r * game.scale, this.r * game.scale, 0, 0, 2 * Math.PI);
        game.ctx.fill();
    }
}