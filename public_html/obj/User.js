import Player from "./Player.js"

export default class User extends Player {
    lastvx = 0
    lastvy = 0
    lastr = 0;
    constructor(obj = null) {
        super();
        if (obj) {
            obj && Object.assign(this, obj);
        }
        this.colour = "blue";
    }

    update(game) {
        this.sizeMultiplier = (1 - (this.r / 300) + 10 / 300);
        if (this.sizeMultiplier < 0.4) {
            this.sizeMultiplier = 0.4;
        }

        game.scale = this.sizeMultiplier;

        this.vx = this.sizeMultiplier * this.maxv * ((game.canvas.width / 2) - game.mouseX) / (game.canvas.width / 2);
        this.vy = this.sizeMultiplier * this.maxv * ((game.canvas.height / 2) - game.mouseY) / (game.canvas.height / 2);

        super.update(game);

        game.xoffset = this.x * game.scale - game.canvas.width / 2;
        game.yoffset = this.y * game.scale - game.canvas.height / 2;

        for (var k in game.dots) {
            let dx = Math.abs(this.x - game.dots[k].x);
            let dy = Math.abs(this.y - game.dots[k].y);
            let dt = Math.sqrt(dx * dx + dy * dy);
            if (dt + game.dots[k].r < this.r) {
                let dotArea = Math.PI * game.dots[k].r * game.dots[k].r;
                let userArea = Math.PI * this.r * this.r;
                userArea = dotArea + userArea;
                this.r = Math.sqrt(userArea / Math.PI);
                delete game.dots[k];
                game.socket.emit("eat", k);
            }
        }

        if (this.lastvx != this.vx || this.lastvy != this.vy || this.lastr == this.r) {
            game.socket.emit("update", this);
        }

        this.lastvx = this.vx;
        this.lastvy = this.vy;
        this.lastr = this.r;
    }

    draw(game) {
        game.ctx.fillStyle = this.colour;
        game.ctx.beginPath();
        game.ctx.ellipse(game.canvas.width / 2, game.canvas.height / 2, this.r * game.scale, this.r * game.scale, 0, 0, 2 * Math.PI);
        game.ctx.fill();
        game.ctx.fillStyle = "black";
        game.ctx.font = "20px sans-serif";
        game.ctx.fillText("X: " + Math.round(this.x) + " Y: " + Math.round(this.y) + " R: " + Math.round(this.r), 25, 25);
    }
}