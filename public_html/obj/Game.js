import Dot from "./Dot.js";
import User from "./User.js";
import Player from "./Player.js";

export default class Game {

    dots = []
    socket;
    canvas;
    ctx;
    WIDTH = 5000;
    HEIGHT = 5000;
    xoffset = 0;
    yoffset = 0;
    mouseX = 0;
    mouseY = 0;
    user;
    scale = 1
    socketid = ""

    constructor() {
        if (window.location.hostname == "localhost") {
            this.socket = io("//" + window.location.hostname + ":3000");
        } else {
            this.socket = io("//" + window.location.hostname);
        }
        this.socket.on("connect", () => {
            this.socket.on("join", (id, dots) => {
                this.socketid = id;
                for (var k in dots) {
                    if (dots[k].vx != undefined) {
                        this.dots[k] = new Player(dots[k]);
                    } else {
                        this.dots[k] = new Dot(dots[k]);
                    }
                }
                this.user = new User(this.dots[id]);
                this.dots[id] = this.user;
            });
        });

        this.socket.on("new-player", (i, player) => {
            if (player != this.user) {
                this.dots[i] = new Player(player);
            }
        });

        this.socket.on("new-dot", (i, dot) => {
            this.dots[i] = new Dot(dot);
        });

        this.socket.on("eat", (i) => {
            if (i == this.socketid) {
                this.user = null;
            }
            delete this.dots[i];
        });

        this.socket.on("update", (i, dot) => {
            this.dots[i] = new Player(dot);
        });

        this.canvas = document.querySelector("#canvas");
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = canvas.getContext("2d");

        document.addEventListener("mousemove", (e) => {
            this.mouseX = e.clientX;
            this.mouseY = e.clientY;
        });

        window.addEventListener("resize", (e) => {
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerHeight;
        });
    }

    drawGameArea() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.fillStyle = "#f4f4f4";
        this.ctx.fillRect(-this.xoffset, -this.yoffset, this.WIDTH * this.scale, this.HEIGHT * this.scale);
    }

    drawDots() {
        for (var k in this.dots) {
            if (k == this.socketid) {
                continue;
            }
            this.dots[k].update(this);
            this.dots[k].draw(this);
        }
        if (this.dots[this.socketid]) {
            this.dots[this.socketid].update(this);
            this.dots[this.socketid].draw(this);
        }
    }

    loop() {
        this.drawGameArea();
        this.drawDots();
    }
}