export default class Dot {
    x = 0;
    y = 0;
    r = 0;
    colour = "green";

    constructor(obj = null) {
        if (obj) {
            obj && Object.assign(this, obj);
        } else {
            this.x = Math.random() * 5000;
            this.y = Math.random() * 5000;
            this.r = 5;
        }
    }

    update(game) {

    }

    draw(game) {
        game.ctx.fillStyle = this.colour;
        game.ctx.beginPath();
        game.ctx.ellipse(this.x * game.scale - game.xoffset, this.y * game.scale - game.yoffset, this.r * game.scale, this.r * game.scale, 0, 0, 2 * Math.PI);
        game.ctx.fill();
    }
}
