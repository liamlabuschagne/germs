# Germs
This is a simple game based on the popular online IO game: agar.io in which circles (in this case germs)
float around a game area and consume each other which is played in real time with other players using a node server
connected with websockets.

## Usage
Start the node server on any linux box with node installed: node -r esm server.js